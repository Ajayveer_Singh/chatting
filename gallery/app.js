var express = require('express');


//var expressValidator = require('express-validator');

var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongose = require('mongoose');
var cors = require('cors')
var morgan = require('morgan');
var config = require('./config/config'); // get our config file
var jwt = require('jsonwebtoken');
var io = require('socket.io')();

var index = require('./routes/index');
var users = require('./routes/users');

var app = express();
app.use(cors());
//app.use(expressValidator);

// view engine setup
var ejs = require('ejs')
app.set('view engine', 'ejs')

app.set('superSecret', config.secret); // secret variable

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan('dev'));

app.use('/', index);
app.use('/users', users);
app.use('/public/uploads', express.static('public/uploads'));

mongose.connect('mongodb://localhost:27017/my_database', function (err, db) {
  if (err) {
    console.log(err);
  }
  else {
    useMongoClient: true
    //app.set('superSecret', config.secret); // secret variable
    console.log('Mongo Working')
  }
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;
