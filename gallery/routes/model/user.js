
var mongose = require('mongoose');
var bcrypt = require('bcryptjs');
var express = require('express');

var Schema = mongose.Schema;
var userSchema = new Schema({

  name: String,
  email: String,
  pass: String,

});

// Omit the password when returning a user
userSchema.set('toJSON', {
  transform: function (doc, ret, options) {
    delete ret.pass;
    return ret;
  }
});


module.exports = mongose.model('User', userSchema);


//export default User;
