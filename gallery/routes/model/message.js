
var mongose = require('mongoose');
var bcrypt = require('bcryptjs');
var User = require('./user')

var express = require('express');

var Schema = mongose.Schema;
var messageSchema = new Schema({

  // to: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  // from: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  to: String,
  from: String,
  message: String,
  roomId: String,
  type: String,
  attachment: String

}, { timestamps: true });

module.exports = mongose.model('Message', messageSchema);


//export default User;
