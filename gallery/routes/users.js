var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser')
var bcrypt = require('bcryptjs');
var dotenv = require('dotenv');

var multer = require('multer')
var upload = multer({ dest: 'uploads/' })
var jwt = require('jsonwebtoken');
var config = require('../config/config'); // get our config file

const v = require('node-input-validator');
var User = require('./model/user');

// var storage = multer.memoryStorage()
// var upload = multer({ storage: storage })

/**
 * Authentication server
 */

router.use(function (req, res, next) {
  
      // check header or url parameters or post parameters for token
      var token = req.body.token || req.query.token || req.headers['x-token'];
  
      // decode token
      if (token) {
  
          // verifies secret and checks exp
          jwt.verify(token,req.app.get('superSecret'), function (err, decoded) {
              if (err) {
                  return res.json({ success: false, message: 'Failed to authenticate token.' });
              }
               else {
             
      // if everything is good, save to request for use in other routes
                  req.decoded = decoded;
                  next();
              }
          });
  
      } else {
            // if there is no token
          // return an error
          return res.status(403).send({
              success: false,
              message: 'No token provided.'
          });
  
      }
  });


/* GET users listing. 
*/
router.get('/', function (req, res, next) {
  User.find(function (err, posts) {
    if (err) { return next(err) }
    res.json(posts)
  })
});

//Updation by id
router.put('/:id', function (req, res, next) {

  User.update({ _id: req.params.id }, req.body, function (err, place) {
    res.send(place);
  });
});

//Remove Data
router.delete('/:_id', function (req, res, next) {
  User.remove({ _id: req.params._id }, function (err) {
    if (err) {
      console.log(err);
    }
    else {
      res.send('ok');
    }
  });
});

//FInd By ID
router.get('/:id', function (req, res, next) {
  User.findOne({ _id: req.params.id }, function (err, story) {
    if (err) {
      console.log(err);
    } else {
      res.json(story);
    }

  });
});




//Login  Method
// router.post('/login/:email/:pass', function (req, res, next) {
//   User.findOne({ email: req.params.email }, function (err, user) {
//     if (!user) {
//       res.status(403).send();
//     }

//     else {
//       user.comparePassword({ pass: req.params.pass }, function (err, isMatch) {
//         if (err) { console.log(err); }

//         if (!isMatch) {
//           res.status(403).send();
//         }
//         else {
//           res.status(200).send();

//         }
//       });
//     }
//   });
// });



module.exports = router;
