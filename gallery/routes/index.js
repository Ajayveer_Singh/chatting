var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser')
var bcrypt = require('bcryptjs');
var dotenv = require('dotenv');
var Message = require('./model/message');

const fs = require('fs');
//require multer for the file uploads
var multer = require('multer');
// set the directory for the uploads to the uploaded to
var DIR = 'public/uploads/';
//define the type of upload multer would be doing and pass in its destination, in our case, its a single file with the name photo
var upload = multer({ dest: DIR }).single('photo');
var filepath = null;
var checkStatus = true;
var http = require('http');
var jwt = require('jsonwebtoken');
var config = require('../config/config'); // get our config file

const v = require('node-input-validator');
var User = require('./model/user');

var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);

server.listen(4000);

// socket io
io.on('connection', function (socket) {
  console.log('User connected');
  socket.on('disconnect', function () {
    console.log('User disconnected');
  });
  socket.on('save-message', function (data) {
    console.log("save")
    console.log(data);
    io.emit('new-message', { message: data });
  });
});


/* GET users listing. 
*/
router.get('/', function (req, res, next) {
  User.find(function (err, posts) {
    if (err) { return res.send(err) }
    else
      res.json(posts)
  })
});



//file upload

router.post('/file', function (req, res, next) {
  var path = '';
  upload(req, res, function (err) {
    if (err) {
      return res.status(422).send("an Error occured")
    }
    else {
      const file = req.file;
      let newFileName = file.originalname;
      newFileName = newFileName.split('.');
      newFileName.splice(newFileName.length - 1, 0, new Date() * 1);
      newFileName = newFileName.join('.');
      const path = newFileName;
      fs.rename('public/uploads/' + file.filename, 'public/uploads/' + newFileName, function (err) {
        if (err) {
          console.log('ERROR: ' + err);
        }
      });
      return res.send(path);
    }
  });
});

//**
//Chat Save  Method
//*/
router.post('/chat', function (req, res, next) {
  console.log(req.body.to);
  var msg = new Message({
    to: req.body.to,
    from: req.body.from,
    roomId: req.body.roomId,
    message: req.body.message,
    attachment: req.body.attachment,
    type: req.body.type
  })
  msg.save(function (err, msg) {
    if (err) { return res.send(err) }
    else res.json(msg);
  })
  // User.findOne({ email: req.body.from }, function (err, user) {
  //   if (!user) { return res.status(401).send('You are not valid user'); }
  //   else {

  //   }
  // });
});

/* GET Chat message all. 
*/
router.get('/chat/:_id', function (req, res, next) {
  console.log(req.params._id)
  Message.find({ $or: [{ 'to': req.params._id }, { 'from': req.params._id }] }).exec(
    function (err, result) {
      if (!err) res.send(result);
      else res.send(err);
    });
});

/* GET Chat message all. 
*/
router.get('/file/down/:data', function (req, res, next) {
  res.download('public/uploads/' +req.params.data, function (err) {
    if (err) {
     res.send(err);
    } else {
      // decrement a download credit, etc.
    }
  });
});


//Create Method
router.post('/', function (req, res, next) {
  let r = {};
  var name = req.body.name;
  var email = req.body.email;
  var pno = req.body.pno;
  var role = req.body.role;
  var pass = req.body.pass;

  let validator = new v(r, req.body, {
    email: 'required|email',
    pass: 'minLength:8'
  });

  validator.check().then(function (matched) {
    if (!matched) {
      res.send(validator.errors);
    }
    else {
      var post = new User({
        name: name,
        email: email,
        pass: pass
      })
      post.save(function (err, post) {
        if (err) { return next(err) }
        res.json(201, post);
        return;
      })
    }
  });
});


//**
//Login  Method
//*/
router.post('/login', function (req, res, next) {
  console.log(req.body);

  User.findOne({ email: req.body.email }, function (err, user) {
    if (!user) { return res.status(401).send('You are not valid user'); }
    else {
      var newpass = req.body.pass
      if (newpass == user.pass) {
        res.json(user);
      } else {
        res.status(401).send('Wrong Password');
      }
    }
  });
});

/**
 * User authentication Api
 */
router.post('/auth', function (req, res) {

  // find the user
  User.findOne({
    email: req.body.email
  }, function (err, user) {

    if (err) throw err;
    else
      if (!user) {
        res.status(403).send({ success: false, message: 'Authentication failed. User not found.' });
      } else if (user) {

        // check if password matches
        if (user.pass != req.body.pass) {
          res.status(403).send({ success: false, message: 'Authentication failed. Wrong password.' });
        } else {

          // if user is found and password is right
          // create a token with only our given payload
          // we don't want to pass in the entire user since that has the password
          const payload = {
            name: user.name,
            //id:user._id
          };
          var token = jwt.sign(payload, req.app.get('superSecret'), {
            expiresIn: '1h'// expires in 1 hours
          });

          // return the information including token as JSON
          res.json({
            user: user,
            token: token
          });
        }

      }

  });
});


module.exports = router;
