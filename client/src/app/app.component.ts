import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { MessageService } from './service/message.service';
import { AuthService} from './service/auth.service';
import { Router } from '@angular/router';
import { User } from './user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  message: any;
  subscription: Subscription;

  constructor(private _messageService: MessageService, private auth:AuthService,
    private router: Router) {
    // subscribe to home component messages
    this.subscription = this._messageService.getMessage()
    .subscribe(message => { this.message = message; });
  }

  ngOnInit(){
   localStorage.getItem('token');

  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
  }

  // userLogout(){
  //   this.auth.logout()  
  // }
  userLogout() {
    localStorage.removeItem('token');
    localStorage.removeItem('loggedIn');
    this.auth.loggedIn = false;
     this.auth.currentUser = new User();
    this.router.navigate(['/login']);
  }

 

  checkLogin(){
    if(localStorage.getItem('loggedIn') == 'true'){
      return false;
    }
    else
    return true;
  }
}
