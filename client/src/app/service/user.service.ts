import { Injectable } from '@angular/core';
import { Http,RequestOptions, Headers, URLSearchParams,  } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { User } from '../user';
import { Response } from '@angular/http';

 @Injectable()
export class UserService {
token:string=localStorage.getItem('token');
  constructor(private _http: Http) { }

  //Register A user
  register(user: User) {
    
    return this._http.post('http://localhost:3000/', user).map((res: Response) => res.json());
  }

  //Fetch the data
  getData():Observable<any> {
    let token=localStorage.getItem('token');

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('x-token', token);
    let options = new RequestOptions({ headers: headers });
    return this._http.get('http://localhost:3000/users/',options).map((res: Response) => res.json());
  }

  //Fetch the User by ID
  getDataById(id):Observable<User>  {
    let token=localStorage.getItem('token');

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('x-token', token);
    let options = new RequestOptions({ headers: headers });
    return this._http.get('http://localhost:3000/users/' + id,options).map((res: Response) => res.json());
  }

  //Update a User
  userUpdate(user: User):Observable<Number>  {
    let token=localStorage.getItem('token');

    let body = JSON.stringify(user);
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('x-token', token);
    let options = new RequestOptions({ headers: headers });
    return this._http.put('http://localhost:3000/users/' + user.id, user,options).map(success => success.status)
  }

  //Delete a User
  deleteUser(id) {
    let token=localStorage.getItem('token');

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('x-token', token);
    let options = new RequestOptions({ headers: headers });
    return this._http.delete('http://localhost:3000/users/' + id,options).map(success => success.status);
  }

  //Login Function
  login(loginData){
    return this._http.post('http://localhost:3000/auth',loginData).map((res:Response) =>res.json());
  }
}
