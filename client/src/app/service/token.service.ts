import {Injectable} from '@angular/core'
import {Http} from '@angular/http';
@Injectable()
export class TokenManager {

    private tokenKey:string = 'app_token';
    constructor(private _http:Http) { }
    
    public store(content:Object) {
        localStorage.setItem(this.tokenKey, JSON.stringify(content));
    }

    public retrieve() {
        let storedToken:string = localStorage.getItem(this.tokenKey);
        if(!storedToken) throw 'no token found';
        return storedToken;
    }

    
}
