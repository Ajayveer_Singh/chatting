import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Response } from '@angular/http';
import { Router } from '@angular/router';
import { User } from '../user';

import { UserService } from './user.service';

@Injectable()
export class AuthService {
  public loggedIn;
  public isAdmin;
  public currentUser: User = new User();

  constructor(private userService: UserService,
    private router: Router) {
    const token = localStorage.getItem('token');
    const loggedIn = localStorage.getItem('loggedIn');
    this.loggedIn = loggedIn;
  }

  login(emailAndPassword) {
    return this.userService.login(emailAndPassword).map(
      res => {
        console.log(res);
        localStorage.setItem('currentUser', JSON.stringify(res.user));

        localStorage.setItem('token', res.token);
        if(res.success){
          localStorage.setItem("loggedIn",'true');
        }
        return this.loggedIn;
      }
    );
  }


  

}
