import { Injectable } from '@angular/core';
import { Router, CanActivate, CanActivateChild } from '@angular/router';
import { CanDeactivate } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private auth: AuthService, public router: Router) { }

  canActivate() {
    if (!this.auth.loggedIn) {
      this.router.navigate(['login']);
      return false;
    }
    else
      return true;
  }


}