import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class ChatService {
  baseUrl = 'http://localhost:3000/';
  constructor(private http: Http) { }

  getChatByRoom(room) {
    return new Promise((resolve, reject) => {
      this.http.get(this.baseUrl + room)
        .map(res => res.json())
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  saveChat(data) {
    return new Promise((resolve, reject) => {
      this.http.post('/', data)
        .map(res => res.json())
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  sendChat(data) {
    return this.http.post(this.baseUrl + 'chat', data).map(
      res => {
        return res.json();
      }

    )
  }
  receiveChat(data) {
    return this.http.get(this.baseUrl + 'chat/' + data).map(
      res => {
        return res.json();
      }

    )
  }
  getUsers() {
    return this.http.get(this.baseUrl).map(
      res => {
        return res.json();
      }

    )
  }

  attachmentUpload(data) {
    return this.http.post(this.baseUrl + 'file', data).map(
      res => {
        return res;

      }
    )
  }

  downloadChatAttachment(data) {
    return this.http.get(this.baseUrl + 'file/down' + data).map(
      res => {
        return res;
      }
    )
  }
}