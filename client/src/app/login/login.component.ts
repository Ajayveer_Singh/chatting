import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../service/auth.service';
import { User } from '../user';
import { MessageService } from '../service/message.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  data: any[];
  user = new User();
  message: string;
  message1: string = "";
  checkUser: boolean;

  constructor(private _messageService: MessageService,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit() {
    
    this.clearMessage();
    this.checkLogin()
  }

  authLogin(user: any) {

    this.authService.login(user).subscribe(
      res => {
        localStorage.setItem("loggedIn","true");
        console.log(".........")
        this.router.navigateByUrl('/chat');
      },
      error => {
        this.message1 = "Invalid Email or Password!";

      },
    );

  }
  sendMessage() {
    this._messageService.sendMessage(this.message);
  }
  clearMessage() {
    this._messageService.clearMessage();
  }
  checkLogin(){
    let token=localStorage.getItem('token');
   
 
  }
}
