import { User} from './user';

export  class Message{
    public to:User;
    public from:User;
    public roomId:number;
    public message?:string;
 
 }
