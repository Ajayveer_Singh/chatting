import { Component, OnInit, OnDestroy, ViewChildren, ViewChild, AfterViewInit, QueryList, ElementRef } from '@angular/core';

import { SocketService } from '../service/socket.service';
import { Subscription } from 'rxjs/Subscription';
import * as io from "socket.io-client";
import { ChatService } from '../service/chat.service';
import { Message } from '../model/message';
import { UserService } from '../service/user.service'
@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit, AfterViewInit, OnDestroy {
  sub: Subscription;
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  users: any;
  chats: any;
  showChat: any;
  msgObj = { roomId: "", to: "", from: "", message: '', type: '', attachment: '' }
  socket = io('http://localhost:4000');
  selectedItem: any;
  currentUser: any;
  files: FileList;
  constructor(private socketService: SocketService,
    private chatService: ChatService,
    private userService: UserService,

  ) { }

  ngAfterViewInit(): void {

  }

  ngOnDestroy() {
    // this.sub.unsubscribe();
  }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.getAllUsers();
    this.receive();

    this.socket.on('new-message', (data) => {
      var temp;
      temp = this.chats;
      temp.push(data.message)
      this.chats = temp;
      this.listClick(this.selectedItem)
      this.scrollToBottom();
    });
  }

  getAllUsers() {
    this.chatService.getUsers().subscribe(
      res => {
        this.users = res.filter((obj) => obj.name != this.currentUser.name);
      }
    )
  }

  listClick(newValue) {
    this.selectedItem = newValue;
    if (this.chats)
      this.showChat = this.chats.filter((item) => item.to == newValue || item.from == newValue);

    if (this.showChat.length == 0) {
      let random = Math.floor(Math.random() * (999999 - 100000)) + 100000;
      this.msgObj.roomId = random.toString();
    }
    else {
      this.msgObj.roomId = this.showChat[0].roomId;
    }
  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  receive() {
    this.chatService.receiveChat(this.currentUser.name).subscribe(
      res => {
        this.chats = res;
        this.listClick(this.users[0].name)
        console.log(this.chats);
      }
    )
  }

  sendMessage(data) {
    this.msgObj.message = data.target.value;
    this.msgObj.from = this.currentUser.name;
    this.msgObj.to = this.selectedItem;
    this.msgObj.type = 'message';
    this.msgObj.attachment = "";
    if (this.msgObj.message != "") {
      this.sendChat();
    }
  }

  sendChat() {
    this.chatService.sendChat(this.msgObj).subscribe((result) => {
      this.msgObj = { roomId: result.roomId, to: "", from: "", message: '', type: '', attachment: '' }
      this.socket.emit('save-message', result);
    }, (err) => {
      console.log(err);
    });
  }

  //Get files
  getFiles(event) {
    if (event) {
      this.files = event.target.files;
      this.upload();
    }
  }

  //Upload the user profile
  upload() {
    if (this.files) {
      let fileCount: number = this.files.length;
      let formData = new FormData();
      if (fileCount > 0) { // a file was selected
        for (let i = 0; i < fileCount; i++) {
          formData.append('photo', this.files.item(i));
        }
        this.chatService.attachmentUpload(formData).subscribe(
          res => {
            this.msgObj.message = "";
            this.msgObj.from = this.currentUser.name;
            this.msgObj.to = this.selectedItem;
            this.msgObj.type = 'attachment';
            this.msgObj.attachment = (res._body)
            console.log(res);
            console.log(this.msgObj);
            this.sendChat();
            let newFileList: FileList;
            this.files = newFileList;
            // this.picture = ;
          },
          err => {
            //  this.failureMsg = "Failed";
            //    setTimeout(() => { this.failureMsg = ''; }, 3000);
            //}
            // );
          })
      }
    }
  }

  downloadChatAttachment(fileName) {
    window.location.href = "http://localhost:3000/file/down/" + fileName;
  }

  scrollToBottom(): void {
    try {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch (err) { }
  }

}

