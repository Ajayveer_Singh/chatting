import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//import { FormBuilder, FormControl, FormGroup, Validators, NG_VALIDATORS, } from '@angular/forms';
import { UserService } from '../service/user.service';
import { User } from '../user';
import { error } from 'selenium-webdriver';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
user= new User();
userRole=["cust"];
  constructor(private router: Router,

    private userService: UserService) { }

  ngOnInit() {
//this.getUserData();
  }

  registerUser(user:User) {
  
    this.userService.register(user).subscribe(
      res => {
        this.router.navigate(['/login']);
      },
      error => console.log(error)
    );
  }

  onFileChange(event) {
    if(event.target.files.length > 0) {
      let file = event.target.files[0];
    }
  }



}
