import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ModalModule } from "ngx-modal";
import { AlertModule } from 'ng2-bootstrap/ng2-bootstrap'
import { Ng2OrderModule } from 'ng2-order-pipe'
import { NgxPaginationModule } from 'ngx-pagination'; // <-- import the module
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { UserService } from './service/user.service';
import { AuthService } from './service/auth.service';
import { MessageService } from './service/message.service';
import { AuthGuard } from './service/authguard.service';


import { TokenManager } from './service/token.service';
import { NavbarComponent } from './navbar/navbar.component';
import { LogoutComponent } from './logout/logout.component';
import { ChatComponent } from './chat/chat.component';
import { SocketService } from './service/socket.service';
import { ChatService } from './service/chat.service'
@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    NavbarComponent,
    LogoutComponent,
    ChatComponent,

  ],
  imports: [
    BrowserModule, FormsModule, BrowserAnimationsModule,
    HttpModule, ReactiveFormsModule, ModalModule, Ng2OrderModule,
    NgxPaginationModule, Ng2SearchPipeModule,

    RouterModule.forRoot([
      {
        path: '',
        // canActivate: [AuthGuard],
        component: RegisterComponent
      },
      {
        path: 'navbar',
        canActivate: [AuthGuard],
        component: NavbarComponent
      },
      {
        path: 'chat',
      //  canActivate: [AuthGuard],
        component: ChatComponent

      },
      {
        path: 'register',
        component: RegisterComponent
      },
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'logout',
        component: LogoutComponent
      },



    ])
  ],
  providers: [UserService, TokenManager,
    AuthService, MessageService, AuthGuard,SocketService,ChatService
    // ImagesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
