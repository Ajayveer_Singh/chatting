import { Component, OnInit, OnDestroy} from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, NG_VALIDATORS, } from '@angular/forms';
import { Subscription} from 'rxjs/Subscription';
import {MessageService} from '../service/message.service';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, OnDestroy {

  powers = ['Really Smart', 'Super Flexible', 'Weather Changer'];
name:string;
  hero = {name: 'Dr.', college: '', power: this.powers[0]};
  constructor(private _messageService: MessageService) {
  }
  ngOnInit(){}

  ngOnDestroy() {
 
  }
}